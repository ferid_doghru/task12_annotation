package Calendar;

import Annotation.AnnotationTask;

@AnnotationTask(name="November",day_of_month = 12)
public class Calendar {
    public Calendar(){}
    public void printAnnotation() {
        AnnotationTask task = this.getClass().getAnnotation(AnnotationTask.class);
        System.out.println(((AnnotationTask)task).name());
        System.out.println(((AnnotationTask)task).day_of_month());
    }
}
